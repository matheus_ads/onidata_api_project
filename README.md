Project test for dev role in Onidata.

Using Django, create a simple API that allows users to post and retrieve their reviews.

Acceptance Criteria

* Users are able to submit reviews to the API
* Users are able to retrieve reviews that they submitted
* Users cannot see reviews submitted by other users
* Use of the API requires a unique auth token for each user
* Submitted reviews must include, at least, the following attributes:
    * Rating - must be between 1 - 5
    * Title - no more than 64 chars
    * Summary - no more than 10k chars
    * IP Address - IP of the review submitter
    * Submission date - the date the review was submitted
    * Company - information about the company for which the review was submitted, can be simple text (e.g., name, company id, etc.) or a separate model altogether
    * Reviewer Metadata - information about the reviewer, can be simple text (e.g., name, email, reviewer id, etc.) or a separate model altogether
Optional:
* Provide an authenticated admin view that allows me to view review submissions
* Document the API

Organize the schema and data models in whatever manner you think makes the most sense and feel free to add any additional style and flair to the project that you'd like.


#### Project Requirements
- Python 3.6
- Make

Clone project
```
git clone git@gitlab.com:matheus_ads/onidata_api_project.git
```

Make a virtualenv with at least Python3.5
```
virtualenv -p /usr/local/bin/python3.6 venv
```

Activate virtualenv
```
source venv/bin/activate
```

Install dependencies
```
make init
```

Make migrations

```
python manage.py makemigrations core
python manage.py migrate
```

Create a superuser.
Please use username = admin and password = onidata123
Because its hardcoded in postman tests
```
python manage.py creatersuperuser
```

Then finally run server
```
python manage.py runserver
```

Run unit tests

```
make test
```

Run codestyle

```
make code-convention
```

Clean some folders and files
```
make clean
```

Install Postman and import 
```
onidata_api_tests.postman_collection.json
```

TODO's
- Create a docker 
- Change database to MongoDB
- Create a documentation in Swagger or Apiary
- Deploy
