.PHONY: test all
.DEFAULT_GOAL := test

init:
	pip install -r requirements.txt

test:
	pytest -v

test-cov:
    py.test -v --cov-report=term  --cov-report=html --cov=.

code-convention:
	flake8
	pycodestyle

clean:
	rm -rf core/__pycache__
	rm -rf core/tests/__pycache__
	rm -rf onidataproject/__pycache__
	rm -rf reports


