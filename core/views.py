from datetime import datetime

from django.http import QueryDict
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics, permissions
from core.models import Review
from core.serializers import ReviewSerializer
from django.contrib.auth.models import User
from ipware import get_client_ip
from .utils import find_user, create_token


class ReviewList(APIView):
    permission_classes = (permissions.IsAuthenticated, )

    @staticmethod
    def get(request):
        username = find_user(request)
        if username is None:
            reviews = Review.objects.all()
        else:
            reviews = Review.objects.filter(username=username)
        serializer = ReviewSerializer(reviews, many=True)
        return Response(serializer.data)

    @staticmethod
    def post(request):
        username = find_user(request)
        ip, _ = get_client_ip(request)
        submission_date = datetime.now()
        data = request.data.dict() if isinstance(request.data, QueryDict) else request.data
        # workaround cast querydict(immutable) to dict necessary to execute unit test
        data['username'] = username
        data['ip_address'] = ip
        data['reviewer_metadata'] = '{}_{}'.format(ip, submission_date)
        data['submission_date'] = submission_date.strftime('%Y-%m-%d')

        serializer = ReviewSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Register(generics.CreateAPIView):

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        password = request.data.get('password')
        email = request.data.get('email')
        user = User.objects.create_user(username, email, password)
        user.save()
        token = create_token(user)
        return Response({'message': 'User created. Please save your credentials',
                         'access_token': str(token.access_token),
                         'refresh_token': str(token)}, status=status.HTTP_201_CREATED)
