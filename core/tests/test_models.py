from core.serializers import ReviewSerializer


def _add_review_data(review):
    review["username"] = "test_user"
    review["ip_address"] = "127.0.0.1"
    review["submission_date"] = "2018-04-24"
    review["reviewer_metadata"] = "127.0.0.1_2018-04-24 18:32:09.301666"
    return review


def test_create_review(review1):
    review1 = _add_review_data(review1)
    serializer = ReviewSerializer(data=review1)
    assert serializer.is_valid()


def test_create_review_error(review_data_error):
    review_data_error = _add_review_data(review_data_error)
    serializer = ReviewSerializer(data=review_data_error)
    assert not serializer.is_valid()
