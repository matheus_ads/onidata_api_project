import os
import pytest
from django.conf import settings
import django

# We manually designate which settings we will be using in an environment variable
# This is similar to what occurs in the `manage.py`
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'onidataproject.settings')


# `pytest` automatically calls this function once when tests are run.
def pytest_configure():
    settings.DEBUG = False
    django.setup()


@pytest.fixture
def review_data_error():
    return {"rating": 20, "title": "Teste de review com título superior a sessenta e quatro caracteres",
            "summary": "Bom funcionamento do sistema", "company": "Onidata"}


@pytest.fixture
def review1():
    return {"rating": 5, "title": "Bom", "summary": "Bom funcionamento do sistema", "company": "Onidata"}
