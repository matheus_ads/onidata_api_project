import pytest
from onidataproject.settings import SECRET_KEY
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from jose import jwt
from django.test import Client
from core.models import Review

client = Client()


def _assert_token_username(token, username):
    decoded_token = jwt.decode(token, SECRET_KEY, algorithms=['HS256'])
    assert decoded_token.get('username') == username


def _setup_user(user_data, admin_token):
    response = client.post(reverse('register'), user_data, HTTP_AUTHORIZATION='Bearer {}'.format(admin_token))
    _assert_token_username(response.data.get('access_token'), user_data.get('username'))
    return response.data.get('access_token')


def _post_review(user_token, data):
    return client.post(reverse('reviews'), data=data, HTTP_AUTHORIZATION='Bearer {}'.format(user_token))


def _get_reviews(user_token):
    return client.get(reverse('reviews'), HTTP_AUTHORIZATION='Bearer {}'.format(user_token))


@pytest.fixture
def admin_token():
    admin = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
    admin.save()
    data = {'username': admin.username, 'password': 'admin123'}
    response = client.post(reverse('token_obtain_pair'), data, format='json')
    assert response.data.get('refresh')
    assert response.data.get('access')
    return response.data.get('access')


@pytest.fixture
def user1_data():
    return {'username': 'test_user', 'email': 'user@test.com', 'password': 'test123'}


@pytest.fixture
def user2_data():
    return {'username': 'test_user2', 'email': 'user2@test.com', 'password': 'test123'}


@pytest.mark.django_db
def test_register_user(admin_token, user1_data):
    response = client.post(reverse('register'), user1_data, HTTP_AUTHORIZATION='Bearer {}'.format(admin_token))
    users = User.objects.all()
    assert len(users) == 2
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data.get('message') == 'User created. Please save your credentials'
    assert response.data.get('refresh_token')
    assert response.data.get('access_token')
    _assert_token_username(response.data.get('access_token'), 'test_user')


@pytest.mark.django_db
def test_post_reviews_user1(user1_data, admin_token, review1):
    user_token = _setup_user(user1_data, admin_token)
    response = _post_review(user_token, review1)
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data['id'] == 1
    assert response.data['username'] == 'test_user'
    assert response.data['rating'] == review1['rating']
    assert response.data['title'] == review1['title']
    assert response.data['summary'] == review1['summary']
    assert response.data['company'] == review1['company']
    assert response.data['submission_date']
    assert response.data['reviewer_metadata']
    assert response.data['ip_address']
    assert len(Review.objects.all()) == 1


@pytest.mark.django_db
def test_post_reviews_user1_should_fail(user1_data, admin_token, review_data_error):
    """Test with invalid data"""
    user_token = _setup_user(user1_data, admin_token)
    response = _post_review(user_token, review_data_error)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {"rating": ["Ensure this value is less than or equal to 5."],
                             "title": ["Ensure this field has no more than 64 characters."]}
    assert len(Review.objects.all()) == 0


@pytest.mark.django_db
def test_add_reviews_from_different_users(user1_data, user2_data, admin_token, review1):
    user_token = _setup_user(user1_data, admin_token)
    assert len(Review.objects.all()) == 0
    _post_review(user_token, review1)
    assert len(Review.objects.all()) == 1
    _post_review(user_token, review1)
    assert len(Review.objects.all()) == 2

    user2_token = _setup_user(user2_data, admin_token)
    assert len(Review.objects.all()) == 2
    _post_review(user2_token, review1)
    assert len(Review.objects.all()) == 3
    _post_review(user2_token, review1)
    assert len(Review.objects.all()) == 4


@pytest.mark.django_db
def test_get_reviews_from_user(user1_data, admin_token, review1):
    user_token = _setup_user(user1_data, admin_token)
    assert len(Review.objects.all()) == 0
    response = _get_reviews(user_token)
    assert len(response.data) == 0

    _post_review(user_token, review1)
    _post_review(user_token, review1)
    response = _get_reviews(user_token)
    assert len(response.data) == 2
    _assert_token_username(user_token, response.data[0]['username'])
    _assert_token_username(user_token, response.data[1]['username'])


@pytest.mark.django_db
def test_get_reviews_from_differnt_users(user1_data, user2_data, admin_token, review1):
    """Test adding reviews from two diff users and each only seeing your.
       Admin should see all.
    """
    user_token = _setup_user(user1_data, admin_token)
    user2_token = _setup_user(user2_data, admin_token)
    _post_review(user_token, review1)
    _post_review(user_token, review1)

    _post_review(user2_token, review1)
    _post_review(user2_token, review1)
    _post_review(user2_token, review1)

    response = _get_reviews(user_token)
    assert len(response.data) == 2
    _assert_token_username(user_token, response.data[0]['username'])
    _assert_token_username(user_token, response.data[1]['username'])

    response = _get_reviews(user2_token)
    assert len(response.data) == 3
    _assert_token_username(user2_token, response.data[0]['username'])
    _assert_token_username(user2_token, response.data[1]['username'])
    _assert_token_username(user2_token, response.data[2]['username'])

    response = _get_reviews(admin_token)
    assert len(response.data) == 5
