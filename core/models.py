from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Review(models.Model):

    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    title = models.TextField(max_length=64)
    summary = models.CharField(max_length=10000)
    company = models.TextField()
    ip_address = models.GenericIPAddressField(protocol='both')
    submission_date = models.DateField()
    reviewer_metadata = models.TextField(default='abc')
    username = models.TextField(default='admin')
