from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from jose import jwt
from onidataproject.settings import SECRET_KEY


def create_token(user):
    token = TokenObtainPairSerializer().get_token(user)
    token['username'] = user.username
    return token


def get_token(request):
    token = request.META.get('HTTP_AUTHORIZATION')
    return token


def find_user(request):
    token = get_token(request)
    _, token = token.split()
    decoded_token = jwt.decode(token, SECRET_KEY, algorithms=['HS256'])
    return decoded_token.get('username')
